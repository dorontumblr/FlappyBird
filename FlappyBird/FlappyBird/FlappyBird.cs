﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FlappyBird
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class FlappyBird : GameEnvironment
    {
        Level level;
        GameOver gameover;

        public FlappyBird() : base()
        {
            Content.RootDirectory = "Content";
            windowSize = new Point(288, 512);
            Screen = windowSize;
            ApplyResolutionSettings();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            level = new Level();
            gameover = new GameOver();

            GameEnvironment.GameStateManager.AddGameState("level", level);
            GameEnvironment.GameStateManager.AddGameState("gameover", gameover);
            GameEnvironment.GameStateManager.SwitchTo("level");

            base.Initialize();
        }

    }
}
