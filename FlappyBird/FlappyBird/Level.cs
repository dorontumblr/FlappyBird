﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlappyBird
{
    class Level : GameObjectList
    {
        SpriteGameObject background;
        Obstacle obstacle;
        Player player;

        Vector2 initialObstaclePosition;

        public Level(int layer = 0, string id = "") : base(layer, id)
        {
            background = new SpriteGameObject("background-day");
            obstacle = new Obstacle();
            player = new Player("bluebird-midflap");

            Add(background);
            Add(obstacle);
            Add(player);

            initialObstaclePosition = new Vector2(GameEnvironment.Screen.X + obstacle.Width, GameEnvironment.Screen.Y / 2);
            obstacle.Position = initialObstaclePosition;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            checkObstacle();
            checkCollision();
        }

        protected void checkObstacle()
        {
            if (obstacle.Position.X + obstacle.Width < 0)
            {
                obstacle.Position = initialObstaclePosition;
                obstacle.SetObstaclePositions();
            }
        }

        protected void checkCollision()
        {
            if (obstacle.CollidesWith(player)
                || player.Position.Y + player.Height < 0
                || player.Position.Y - player.Height > GameEnvironment.Screen.Y)
            {
                GameEnvironment.GameStateManager.SwitchTo("gameover");
            }
        }

    }
}
